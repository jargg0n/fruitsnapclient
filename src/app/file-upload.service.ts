import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL: string = 'http://localhost:5000';
const PREDICTION_ENDPOINT: string = '/predictor';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private http: HttpClient) { }

  public getPrediction(image: File) {

    const formData: FormData = new FormData();
    formData.append('image', image, image.name)
    
    return this.http.post(API_URL + PREDICTION_ENDPOINT, formData);
  }
}
